import time





class Chess:
    # figurew = ['♔','♕','♖','♗','♘','♙']
    # figureb = ['♚','♛','♜','♝','♞','♟']

    # Фигуры UNICODE
    figureb = ['♖', '♘', '♗', '♕', '♔', '♗', '♘', '♖', '♙']
    figurew = ['♜', '♞', '♝', '♛', '♚', '♝', '♞', '♜', '♟']

    # Для поиска индекса по полю
    map = {
        "A": 0,
        "B": 1,
        "C": 2,
        "D": 3,
        "E": 4,
        "F": 5,
        "G": 6,
        "H": 7
    }

    # Поиск поля по индексу
    mapl = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

    # Информация о фигуре
    info = {
        '♖': 'R',
        '♘': 'N',
        '♗': 'B',
        '♕': 'Q',
        '♔': 'K',
        '♙': 'P',
        '♜': 'r',
        '♞': 'n',
        '♝': 'b',
        '♛': 'q',
        '♚': 'k',
        '♟': 'p'

    }

    # Инициализация переменных
    def __init__(self):
        # Доска
        self.board = []
        # Запись
        self.record = []
        # Шаг назад
        self.back = -2
        # Счётчик
        self.counter = 0
        # Запись игры
        self.read = ""
        # Положение короля
        self.kingpos = ['E1', "E8"]
        # Ракировка чёрных
        self.racirovkab = False
        # Ракировка белых
        self.racirovkaw = False
        # Ходили ли у чёрных ладья1, ладья2, король
        self.bmove = [True, True, True]
        # Ходили ли у белых ладья1, ладья2, король
        self.wmove = [True, True, True]
        self.peshkaw = ""
        self.peshkab = ""
        self.phw = False
        self.phb = False

    # Создание борда
    def fill(self):
        for x in range(8):
            self.board.append([])
            for y in range(8):
                if x == 0:
                    self.board[x].append(self.figureb[y])
                elif x == 1:
                    self.board[x].append(self.figureb[-1])
                elif x == 6:
                    self.board[x].append(self.figurew[-1])
                elif x == 7:
                    self.board[x].append(self.figurew[y])
                else:
                    self.board[x].append('.')
        self.record.append(self.board)

    # Выводит клавиатуру
    def get_brd(self):
        return self.board

    # Записывает борд
    def record_bord(self):
        self.record.append(self.board)

    def backf(self):
        self.board = self.record[self.back]
        self.back -= 1
        self.get_board()

    def forwardf(self):
        try:
            self.board = self.record[self.back]
            self.back += 1
            self.get_board()
        except:
            print("Ты в конце игры")

    # красиво выводит доску
    def get_board(self):
        prnt = ''
        count = 8
        print('   A B C D E F G H   \n')
        for i in self.board:
            prnt += f'{count}  '
            for j in i:
                prnt += f'{j} '
            prnt = prnt.rstrip()
            prnt += f'  {count}'
            prnt += '\n'
            count -= 1
        print(f'{prnt}\n   A B C D E F G H   ')

    # Проигрывает партию с файла
    def play_demo(self):
        f = open("record.txt", 'r', encoding="UTF-8")
        self.get_board()
        time.sleep(1)
        for i in f:
            print(i)
            self.board[8 - int(i.split(" ")[2][1])][self.map[i.split(" ")[2][0].upper()]] = \
                self.board[8 - int(i.split(" ")[1][
                                       1])][self.map[
                    i.split(" ")[1][0].upper()]]
            self.board[8 - int(i.split(" ")[1][1])][self.map[i.split(" ")[1][0].upper()]] = "."
            self.get_board()
            time.sleep(1)

    # Ход белых
    def movew(self):
        self.counter += 1
        self.get_board()
        print("Ход белых!")
        while True:
            try:
                xod = input("Введите ход: ")
                # Проверка на команды
                if xod == "back":
                    self.backf()
                elif xod == "forward":
                    self.forwardf()
                    # Запись в файл
                elif xod == "record":
                    open("record.txt", "w", encoding="UTF-8").write(self.read)
                # Разделение переменных на координаты
                cords = [self.map[xod[0].upper()], int(xod[1])]
                print(cords)
                # Нахождение выбранной фигуры
                figure = self.board[8 - cords[1]][cords[0]]
                # Проверка если человек выбрал фигуру
                if figure in self.figurew:
                    vx = 'Возможные ходы:\n'
                    vxl = []
                    # Ход ПЕШКИ
                    if self.info[figure] == 'p':
                        # Если координата начала
                        if cords[1] == 2:
                            # Делает 2 хода
                            if self.board[8 - cords[1] - 1][cords[0]] == ".":
                                vx += f'{xod[0].upper()}{cords[1] + 1}\n'
                                vxl.append(f'{xod[0].upper()}{cords[1] + 1}')
                            if self.board[8 - cords[1] - 2][cords[0]] == ".":
                                vx += f'{xod[0].upper()}{cords[1] + 2}\n'
                                vxl.append(f'{xod[0].upper()}{cords[1] + 2}')
                                self.phw = True
                            # Если другая координата
                        elif cords[1] < 8:
                            # Делает 1 ход
                            if self.board[8 - cords[1] - 1][cords[0]] == ".":
                                vx += f'{xod[0].upper()}{cords[1] + 1}\n'
                                vxl.append(f'{xod[0].upper()}{cords[1] + 1}')
                        # Проверка на съедание слева
                        if cords[0] == 0:
                            if self.board[8 - cords[1] - 1][cords[0] + 1] != ".":
                                vx += f'{self.mapl[cords[0] + 1]}{cords[1] + 1}\n'
                                vxl.append(f'{self.mapl[cords[0] + 1]}{cords[1] + 1}')
                        # То же самое справа
                        elif cords[0] == 7:
                            if self.board[8 - cords[1] - 1][cords[0] - 1] != ".":
                                vx += f'{self.mapl[cords[0] - 1]}{cords[1] - 1}\n'
                                vxl.append(f'{self.mapl[cords[0] - 1]}{cords[1] - 1}')
                        # Остальные случаи
                        else:
                            if self.board[8 - cords[1] - 1][cords[0] + 1] != ".":
                                vx += f'{self.mapl[cords[0] + 1]}{cords[1] + 1}\n'
                                vxl.append(f'{self.mapl[cords[0] + 1]}{cords[1] + 1}')
                            if self.board[8 - cords[1] - 1][cords[0] + 1] != ".":
                                vx += f'{self.mapl[cords[0] - 1]}{cords[1] + 1}\n'
                                vxl.append(f'{self.mapl[cords[0] - 1]}{cords[1] + 1}')
                        # Если есть ходы
                        if len(vxl) != 0:
                            # Заканчивает цикл
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    # Ход ЛАДИЙ
                    elif self.info[figure] == 'r':
                        # Проверка на ход ладий для ракировки
                        if xod[0].upper() == "A":
                            self.wmove[0] = False
                        elif xod[0].upper() == "H":
                            self.wmove[1] = False
                        enm = 0
                        # цикл проверяющий верхние клетки
                        for i in range(cords[1] - 1, 7):
                            if self.board[6 - i][cords[0]] == '.' or (self.board[6 - i][cords[0]] in self.figureb
                                                                      and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 2}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 2}')
                                if self.board[6 - i][cords[0]] in self.figureb:
                                    enm += 1
                            else:
                                break
                        enm = 0
                        # Нижние
                        for i in range(0, cords[1] - 1):
                            if (self.board[7 - i][cords[0]]) == '.' or (self.board[7 - i][cords[0]] in self.figureb
                                                                        and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 1}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 1}')
                                if self.board[7 - i][cords[0]] in self.figureb:
                                    enm += 1
                        enm = 0
                        # Правые
                        for i in range(cords[0], len(self.board[0])):
                            if self.board[8 - cords[1]][i] == '.' or (self.board[8 - cords[1]][i] in self.figureb
                                                                      and enm == 0):
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figureb:
                                    enm += 1
                        enm = 0
                        # Левые
                        for i in range(0, cords[0]):
                            if self.board[8 - cords[1]][i] == '.' or self.board[8 - cords[1]][i] in self.figureb:
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figureb:
                                    enm += 1
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    # Ход КОНЯ
                    elif self.info[figure] == 'n':
                        # Возможные ходы коня
                        nxod = ['2;1', '2;-1', '-2;1', '-2;-1', '1;2', '1;-2', '-1;2', '-1;-2']
                        for i in nxod:
                            try:
                                # Разделение хода на 2 переменные
                                digit = int(i.split(";")[0])
                                letter = int(i.split(";")[1])

                                # Условия хода коня
                                if 0 <= 8 - cords[1] - digit <= 7 and 0 <= cords[0] + letter <= 7:
                                    f = self.board[8 - cords[1] - digit][cords[0] + letter]
                                    if f == '.' or f in self.figureb:
                                        vx += f'{self.mapl[cords[0] + letter]}{cords[1] + digit}\n'
                                        vxl.append(f'{self.mapl[cords[0] + letter]}{cords[1] + digit}')
                            except:
                                pass
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    # Ход КОРОЛЯ
                    elif self.info[figure] == 'k':
                        # Все ходы короля
                        xod = ['1;1', '1;0', '1;-1', '0;1', '0;-1', '-1;1', '-1;0', '-1;-1']
                        for i in xod:
                            try:
                                # Разделение
                                digit = int(i.split(";")[0])
                                letter = int(i.split(";")[1])

                                # Условия для хода
                                if 0 <= 8 - cords[1] - digit <= 7 and 0 <= cords[0] + letter <= 7:
                                    f = self.board[8 - cords[1] - digit][cords[0] + letter]
                                    if f == '.' or f in self.figureb:
                                        vx += f'{self.mapl[cords[0] + letter]}{cords[1] + digit}\n'
                                        vxl.append(f'{self.mapl[cords[0] + letter]}{cords[1] + digit}')
                            except:
                                pass
                        # Проверка ходил ли король для ракировки
                        if self.wmove[2]:
                            # Проверка на свободность ячеек
                            if self.board[8 - cords[1]][cords[0] + 1] == "." and self.board[8 - cords[1]][
                                cords[0] + 2] == '.' and self.wmove[1]:
                                vx += "H1\n"
                                vxl.append("H1")
                                # Включает ракировку
                                self.racirovkaw = True
                            # То же самое для другой стороны
                            elif self.board[8 - cords[1]][cords[0] - 1] == "." and self.board[8 - cords[1]][
                                cords[0] - 2] == \
                                    "." and self.board[8 - cords[1]][cords[0] - 3] == "." and self.wmove[0]:
                                vx += "A1\n"
                                vxl.append("A1")
                                self.racirovkaw = True
                        # Обозначает, что король походил
                        self.wmove[2] = False
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    # Ход ФЕРЗЯ
                    elif self.info[figure] == "b":
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] + 1, 9):
                            if (cords[0] + counter <= 7) and rdy[0]:
                                f = self.board[8 - i][cords[0] + counter]
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                                            enm[0] = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                            if (cords[0] - counter >= 0) and rdy[1]:
                                f = self.board[8 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                            counter += 1
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] - 2, 0, -1):
                            if (cords[0] + counter <= 7) and rdy[0]:
                                f = self.board[7 - i][cords[0] + counter]
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i + 1}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i + 1}')
                                            enm[0] = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i + 1}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i + 1}')
                            if (cords[0] - counter >= 0) and rdy[1]:
                                f = self.board[7 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i + 1}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i + 1}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i + 1}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i + 1}')
                            counter += 1
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    # ХОД КОРОЛЕВЫ
                    elif self.info[figure] == 'q':
                        # Сочетание ферзя и ладьи
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] + 1, 9):
                            if cords[0] + counter <= 7 and rdy[0]:
                                f = self.board[8 - i][cords[0] + counter]
                                print(f)
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                                            enm = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                            if cords[0] - counter >= 0 and rdy[1]:
                                f = self.board[8 - i][cords[0] - counter]
                                print(f)
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                            counter += 1
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] - 2, 0, -1):
                            if (cords[0] + counter <= 7) and rdy[0]:
                                f = self.board[7 - i][cords[0] + counter]

                                if f != '.':
                                    if f in self.figureb:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i + 1}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i + 1}')
                                            enm[0] = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i + 1}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i + 1}')
                            if (cords[0] - counter >= 0) and rdy[1]:
                                f = self.board[7 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figureb:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i + 1}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i + 1}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i + 1}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i + 1}')
                            counter += 1
                        enm = 0
                        for i in range(cords[1] - 1, 7):
                            if self.board[6 - i][cords[0]] == '.' or (self.board[6 - i][cords[0]] in self.figureb
                                                                      and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 2}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 2}')
                                if self.board[6 - i][cords[0]] in self.figureb:
                                    enm += 1
                            else:
                                break
                        enm = 0
                        for i in range(0, cords[1] - 1):
                            print(f)
                            if (self.board[7 - i][cords[0]]) == '.' or (self.board[7 - i][cords[0]] in self.figureb
                                                                        and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 1}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 1}')
                                if self.board[7 - i][cords[0]] in self.figureb:
                                    enm += 1
                        enm = 0
                        for i in range(cords[0], len(self.board[0])):
                            print(f)
                            if self.board[8 - cords[1]][i] == '.' or (self.board[8 - cords[1]][i] in self.figureb
                                                                      and enm == 0):
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figureb:
                                    enm += 1
                        enm = 0
                        for i in range(0, cords[0]):
                            print(f)
                            if self.board[8 - cords[1]][i] == '.' or self.board[8 - cords[1]][i] in self.figureb:
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figureb:
                                    enm += 1
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
            except Exception as e:
                print(e)
                print("Введите ячейку в формате <БУКВА><ЧИСЛО>")
        print(vx)
        # Запись для файла
        self.read += f"W {xod} "
        # Копирование доски
        hints = self.get_brd().copy()
        print(vxl)
        # Отмечаем возможные ходы на доске
        for i in vxl:
            if hints[8 - int(i[1])][self.map[i[0]]] == ".":
                hints[8 - int(i[1])][self.map[i[0]]] = "*"
            else:
                hints[8 - int(i[1])][self.map[i[0]]] += "*"
        # Красивый вывод
        prnt = ''
        count = 8
        print('   A B C D E F G H   \n')
        for i in hints:
            prnt += f'{count}  '
            for j in i:
                prnt += f'{j} '
            prnt = prnt.rstrip()
            prnt += f'  {count}'
            prnt += '\n'
            count -= 1
        print(f'{prnt}\n   A B C D E F G H   ')
        print("->")

        # Очистка от звёзд
        for i in vxl:
            if hints[8 - int(i[1])][self.map[i[0]]] == "*":
                hints[8 - int(i[1])][self.map[i[0]]] = "."
            else:
                hints[8 - int(i[1])][self.map[i[0]]] = hints[8 - int(i[1])][self.map[i[0]]][0]

        while True:
            try:
                # Выбор хода
                to = input("Куда ходить: ")
                cordt = [self.map[to[0].upper()], int(to[1])]
                to = to[0].upper() + to[1]
                # Если в списке возможных ходов, даёт пойти
                if to in vxl:
                    if self.phw:
                        self.peshkaw = f'{to[0]}{int(to[1])-1}'
                        self.phw = False
                    else:
                        self.peshkaw = ""
                    break
                print("Введите ячейку в формате <БУКВА><ЧИСЛО> из возможных вариантов!")
            except:
                print("Введите ячейку в формате <БУКВА><ЧИСЛО> из возможных вариантов!")
        # Запись для файла
        self.read += f"{to}\n"
        # Проверка на ракировку
        if self.racirovkaw:
            # Проверка на разные ходы
            if to == "H1":
                # Ракировка по правилам
                self.board[8 - cords[1]][cords[0] + 1] = self.board[8 - cords[1]][cords[0] + 3]
                self.board[8 - cords[1]][cords[0] + 2] = self.board[8 - cords[1]][cords[0]]
                self.board[8 - cords[1]][cords[0] + 3] = "."
                self.board[8 - cords[1]][cords[0]] = "."
            elif to == "A1":
                self.board[8 - cords[1]][cords[0] - 1] = self.board[8 - cords[1]][cords[0] - 4]
                self.board[8 - cords[1]][cords[0] - 2] = self.board[8 - cords[1]][cords[0]]
                self.board[8 - cords[1]][cords[0] - 4] = "."
                self.board[8 - cords[1]][cords[0]] = "."
            self.racirovkaw = False
        elif to == self.peshkab:
            self.board[8 - cords[1]][cords[0]] = '.'
            self.board[8 - cordt[1] - 1][cordt[0]] = "."
            self.board[8 - cordt[1]][cordt[0]] = figure
        else:
            # Обычный ход
            self.board[8 - cords[1]][cords[0]] = '.'
            self.board[8 - cordt[1]][cordt[0]] = figure
            if to == self.kingpos[1]:
                return "W"
        if self.info[figure] == 'k':
            self.kingpos[0] = to
        # Если пешка дошла до конца, то она становится любой фигурой
        if cordt[1] == 8 and self.info[figure] == 'p':
            while True:
                chose = input("Введите на что хотите поменять пешку\nR N B Q\n")
                if chose.lower() in ['r', 'n', 'b', 'q']:
                    self.board[8 - cordt[1]][cordt[0]] = [k for k, v in self.info.items() if v == chose.lower()][0]
                    break
        self.get_board()
        self.record_bord()

    # то же самое, что у белых
    def moveb(self):
        self.counter += 1
        self.get_board()
        print("Ход чёрных!")
        while True:
            try:
                xod = input("Введите ход: ")
                if xod == "back":
                    self.backf()
                elif xod == "forward":
                    self.forwardf()
                elif xod == "record":
                    open("record.txt", "w", encoding="UTF-8").write(self.read)
                cords = [self.map[xod[0].upper()], int(xod[1])]
                print(cords)
                figure = self.board[8 - cords[1]][cords[0]]
                if figure in self.figureb:
                    vx = 'Возможные ходы:\n'
                    vxl = []
                    if self.info[figure] == 'P':
                        if cords[1] == 7:
                            if self.board[8 - cords[1] + 1][cords[0]] == ".":
                                vx += f'{xod[0].upper()}{cords[1] - 1}\n'
                                vxl.append(f'{xod[0].upper()}{cords[1] - 1}')
                            if self.board[8 - cords[1] + 2][cords[0]] == ".":
                                vx += f'{xod[0].upper()}{cords[1] - 2}\n'
                                vxl.append(f'{xod[0].upper()}{cords[1] - 2}')
                                self.phb = True
                        elif cords[1] < 8:
                            if self.board[8 - cords[1] + 1][cords[0]] == ".":
                                vx += f'{xod[0].upper()}{cords[1] - 1}\n'
                                vxl.append(f'{xod[0].upper()}{cords[1] - 1}')

                        if cords[0] == 0:
                            if self.board[8 - cords[1] + 1][cords[0] + 1] != "." and self.board[8 - cords[1] + 1][
                                cords[0] + 1] in self.figurew:
                                vx += f'{self.mapl[cords[0] + 1]}{cords[1] - 1}\n'
                                vxl.append(f'{self.mapl[cords[0] + 1]}{cords[1] - 1}')
                        elif cords[0] == 7:
                            if self.board[8 - cords[1] + 1][cords[0] - 1] != "." and self.board[8 - cords[1] + 1][
                                cords[0] - 1] in self.figurew:
                                vx += f'{self.mapl[cords[0] - 1]}{cords[1] + 1}\n'
                                vxl.append(f'{self.mapl[cords[0] - 1]}{cords[1] + 1}')
                        else:
                            if self.board[8 - cords[1] + 1][cords[0] + 1] != "." and self.board[8 - cords[1] + 1][
                                cords[0] + 1] in self.figurew:
                                vx += f'{self.mapl[cords[0] + 1]}{cords[1] - 1}\n'
                                vxl.append(f'{self.mapl[cords[0] + 1]}{cords[1] - 1}')
                            if self.board[8 - cords[1] + 1][cords[0] + 1] != "." and self.board[8 - cords[1] + 1][
                                cords[0] + 1] in self.figurew:
                                vx += f'{self.mapl[cords[0] - 1]}{cords[1] - 1}\n'
                                vxl.append(f'{self.mapl[cords[0] - 1]}{cords[1] - 1}')
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    elif self.info[figure] == 'R':
                        if xod[0].upper() == "A":
                            self.bmove[0] = False
                        elif xod[0].upper() == "H":
                            self.bmove[1] = False
                        enm = 0
                        for i in range(cords[1] - 1, 7):
                            if self.board[6 - i][cords[0]] == '.' or (self.board[6 - i][cords[0]] in self.figurew
                                                                      and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 2}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 2}')
                                if self.board[6 - i][cords[0]] in self.figurew:
                                    enm += 1
                            else:
                                break
                        enm = 0
                        for i in range(0, cords[1] - 1):
                            if (self.board[7 - i][cords[0]]) == '.' or (self.board[7 - i][cords[0]] in self.figurew
                                                                        and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 1}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 1}')
                                if self.board[7 - i][cords[0]] in self.figurew:
                                    enm += 1
                        enm = 0
                        for i in range(cords[0], len(self.board[0])):
                            if self.board[8 - cords[1]][i] == '.' or (self.board[8 - cords[1]][i] in self.figurew
                                                                      and enm == 0):
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figurew:
                                    enm += 1
                        enm = 0
                        for i in range(0, cords[0]):
                            if self.board[8 - cords[1]][i] == '.' or self.board[8 - cords[1]][i] in self.figurew:
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figurew:
                                    enm += 1
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    elif self.info[figure] == 'N':
                        nxod = ['2;1', '2;-1', '-2;1', '-2;-1', '1;2', '1;-2', '-1;2', '-1;-2']
                        for i in nxod:
                            try:
                                digit = int(i.split(";")[0])
                                letter = int(i.split(";")[1])

                                if 0 <= 8 - cords[1] - digit <= 7 and 0 <= cords[0] + letter <= 7:
                                    f = self.board[8 - cords[1] - digit][cords[0] + letter]
                                    if f == '.' or f in self.figurew:
                                        vx += f'{self.mapl[cords[0] + letter]}{cords[1] + digit}\n'
                                        vxl.append(f'{self.mapl[cords[0] + letter]}{cords[1] + digit}')
                            except:
                                pass
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    elif self.info[figure] == 'K':
                        xod = ['1;1', '1;0', '1;-1', '0;1', '0;-1', '-1;1', '-1;0', '-1;-1']
                        for i in xod:
                            try:
                                digit = int(i.split(";")[0])
                                letter = int(i.split(";")[1])

                                if 0 <= 8 - cords[1] - digit <= 7 and 0 <= cords[0] + letter <= 7:
                                    f = self.board[8 - cords[1] - digit][cords[0] + letter]
                                    if f == '.' or f in self.figurew:
                                        vx += f'{self.mapl[cords[0] + letter]}{cords[1] + digit}\n'
                                        vxl.append(f'{self.mapl[cords[0] + letter]}{cords[1] + digit}')
                            except:
                                pass
                            if self.bmove[2]:
                                if self.board[8 - cords[1]][cords[0] + 1] == "." and self.board[8 - cords[1]][
                                    cords[0] + 2] == '.' and self.bmove[1]:
                                    vx += "H8\n"
                                    vxl.append("H8")
                                    self.racirovkab = True
                                elif self.board[8 - cords[1]][cords[0] - 1] == "." and self.board[8 - cords[1]][
                                    cords[0] - 2] == "." and self.board[8 - cords[1]][cords[0] - 3] == "." and \
                                        self.bmove[0]:
                                    vx += "A8\n"
                                    vxl.append("A8")
                                    self.racirovkab = True
                            self.bmove[2] = False
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    elif self.info[figure] == "B":
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] + 1, 9):
                            if (cords[0] + counter <= 7) and rdy[0]:
                                f = self.board[8 - i][cords[0] + counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                                            enm[0] = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                            if (cords[0] - counter >= 0) and rdy[1]:
                                f = self.board[8 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                            counter += 1
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1]-2, 0, -1):
                            if (cords[0] + counter <= 7) and rdy[0]:
                                f = self.board[7 - i][cords[0] + counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i+1}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i+1}')
                                            enm[0] = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i+1}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i+1}')
                            if (cords[0] - counter >= 0) and rdy[1]:
                                f = self.board[7 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i+1}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i+1}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i+1}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i+1}')
                            counter += 1
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
                    elif self.info[figure] == 'Q':
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] + 1, 9):
                            if cords[0] + counter <= 7 and rdy[0]:
                                f = self.board[8 - i][cords[0] + counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                                            enm = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i}')
                            if cords[0] - counter >= 0 and rdy[1]:
                                f = self.board[8 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i}')
                            counter += 1
                        counter = 1
                        rdy = [True, True]
                        enm = [True, True]
                        for i in range(cords[1] - 2, 0, -1):
                            if (cords[0] + counter <= 7) and rdy[0]:
                                f = self.board[7 - i][cords[0] + counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[0]:
                                            vx += f'{self.mapl[cords[0] + counter]}{i + 1}\n'
                                            vxl.append(f'{self.mapl[cords[0] + counter]}{i + 1}')
                                            enm[0] = False
                                    else:
                                        rdy[0] = False
                                else:
                                    vx += f'{self.mapl[cords[0] + counter]}{i + 1}\n'
                                    vxl.append(f'{self.mapl[cords[0] + counter]}{i + 1}')
                            if (cords[0] - counter >= 0) and rdy[1]:
                                f = self.board[7 - i][cords[0] - counter]
                                if f != '.':
                                    if f in self.figurew:
                                        if enm[1]:
                                            vx += f'{self.mapl[cords[0] - counter]}{i + 1}\n'
                                            vxl.append(f'{self.mapl[cords[0] - counter]}{i + 1}')
                                            enm[1] = False
                                    else:
                                        rdy[1] = False
                                else:
                                    vx += f'{self.mapl[cords[0] - counter]}{i + 1}\n'
                                    vxl.append(f'{self.mapl[cords[0] - counter]}{i + 1}')
                            counter += 1
                        enm = 0
                        for i in range(cords[1] - 1, 7):
                            if self.board[6 - i][cords[0]] == '.' or (self.board[6 - i][cords[0]] in self.figurew
                                                                      and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 2}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 2}')
                                if self.board[6 - i][cords[0]] in self.figurew:
                                    enm += 1
                            else:
                                break
                        enm = 0
                        for i in range(0, cords[1] - 1):
                            if (self.board[7 - i][cords[0]]) == '.' or (self.board[7 - i][cords[0]] in self.figurew
                                                                        and enm == 0):
                                vx += f'{self.mapl[cords[0]]}{i + 1}\n'
                                vxl.append(f'{self.mapl[cords[0]]}{i + 1}')
                                if self.board[7 - i][cords[0]] in self.figurew:
                                    enm += 1
                        enm = 0
                        for i in range(cords[0], len(self.board[0])):
                            if self.board[8 - cords[1]][i] == '.' or (self.board[8 - cords[1]][i] in self.figurew
                                                                      and enm == 0):
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figurew:
                                    enm += 1
                        enm = 0
                        for i in range(0, cords[0]):
                            if self.board[8 - cords[1]][i] == '.' or self.board[8 - cords[1]][i] in self.figurew:
                                vx += f'{self.mapl[i]}{cords[1]}\n'
                                vxl.append(f'{self.mapl[i]}{cords[1]}')
                                if self.board[8 - cords[1]][i] in self.figurew:
                                    enm += 1
                        if len(vxl) != 0:
                            break
                        else:
                            print("Для этой фигуры нет ходов!")
            except Exception as e:
                print(e)
                print("Введите ячейку в формате <БУКВА><ЧИСЛО>")
        self.read += f"B {xod} "
        print(vx)
        hints = self.get_brd().copy()
        for i in vxl:
            if hints[8 - int(i[1])][self.map[i[0]]] == ".":
                hints[8 - int(i[1])][self.map[i[0]]] = "*"
            else:
                hints[8 - int(i[1])][self.map[i[0]]] += "*"
        prnt = ''
        count = 8
        print('   A B C D E F G H   \n')
        for i in hints:
            prnt += f'{count}  '
            for j in i:
                prnt += f'{j} '
            prnt = prnt.rstrip()
            prnt += f'  {count}'
            prnt += '\n'
            count -= 1
        print(f'{prnt}\n   A B C D E F G H   ')
        print("->")

        for i in vxl:
            if hints[8 - int(i[1])][self.map[i[0]]] == "*":
                hints[8 - int(i[1])][self.map[i[0]]] = "."
            else:
                hints[8 - int(i[1])][self.map[i[0]]] = hints[8 - int(i[1])][self.map[i[0]]][0]


        while True:
            try:
                to = input("Куда ходить: ")
                cordt = [self.map[to[0].upper()], int(to[1])]
                to = to[0].upper() + to[1]
                if to in vxl:
                    if self.phb:
                        self.peshkab = f'{to[0]}{int(to[1])-1}'
                        self.phb = False
                    else:
                        self.peshkab = ""
                    break
                print("Введите ячейку в формате <БУКВА><ЧИСЛО> из возможных вариантов!")
            except:
                print("Введите ячейку в формате <БУКВА><ЧИСЛО> из возможных вариантов!")
        self.read += f"{to}\n"
        if self.racirovkab:
            if to == "H8":
                self.board[8 - cords[1]][cords[0] + 1] = self.board[8 - cords[1]][cords[0] + 3]
                self.board[8 - cords[1]][cords[0] + 2] = self.board[8 - cords[1]][cords[0]]
                self.board[8 - cords[1]][cords[0] + 3] = "."
                self.board[8 - cords[1]][cords[0]] = "."
            elif to == "A8":
                self.board[8 - cords[1]][cords[0] - 1] = self.board[8 - cords[1]][cords[0] - 4]
                self.board[8 - cords[1]][cords[0] - 2] = self.board[8 - cords[1]][cords[0]]
                self.board[8 - cords[1]][cords[0] - 4] = "."
                self.board[8 - cords[1]][cords[0]] = "."
        elif to == self.peshkaw:
            self.board[8 - cords[1]][cords[0]] = '.'
            self.board[8 - cordt[1] - 1][cordt[0]] = "."
            self.board[8 - cordt[1]][cordt[0]] = figure
        else:
            self.board[8 - cords[1]][cords[0]] = '.'
            self.board[8 - cordt[1]][cordt[0]] = figure
            if to == self.kingpos[0]:
                return "B"
        if self.info[figure] == 'K':
            self.kingpos[1] = to
        if cordt[1] == 1 and self.info[figure] == 'P':
            while True:
                chose = input("Введите на что хотите поменять пешку\nR N B Q\n")
                if chose.lower() in ['r', 'n', 'b', 'q']:
                    self.board[8 - cordt[1]][cordt[0]] = [k for k, v in self.info.items() if v == chose.lower()][0]
                    break
        self.get_board()
        self.record_bord()


if __name__ == '__main__':
    # Создание класса
    chess = Chess()
    # Заполнение поля
    chess.fill()
    # Выбор типа
    while True:
        chose = input("Что вы хотите сделать:\n[1] - Посмотреть повтор\n[2] - Начать игру\nНужно ввести цифру:")
        if chose == "1":
            # Воспроизведение демо
            chess.play_demo()
        elif chose == "2":
            # Игра
            while True:
                if chess.movew() == "W":
                    print("Чёрный выиграли!")
                    break
                if chess.moveb() == "B":
                    print("Чёрный выиграли!")
                    break

